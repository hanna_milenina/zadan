
package xor;

public class Xor {

   

    double[] enters;//wchody
    double[] hidden;//skrytye nejrony
    double outer; //wychod
    double[][] nej_ent_hid;
    double[] nej_hid_ent;
    double [][] patterns = {
        {0,0},{1,0},{0,1},{1,1}
    };
    double [] answer = {0,1,1,0};


    Xor()
    {
       enters = new double[patterns[0].length];
       hidden = new double[2];
       nej_ent_hid = new double[enters.length][hidden.length];
       nej_hid_ent = new double[hidden.length];
       initW();
       Study();
       
        for(int p =0; p<patterns.length; p++)
            {
                for(int i =0; i<enters.length;i++)
                {
                    enters[i] = patterns[p][i];
                }
                CoutOuter();
                
                System.out.println(outer);
            }
    
    }
    public void initW()
    {
        for(int i =0; i<nej_ent_hid.length; i++)
        {
            for(int j =0; j<nej_ent_hid[i].length;j++ )
            {
                nej_ent_hid[i][j]=Math.random()*0.3+0.1;
                
            }
        }
        for(int i =0; i<nej_hid_ent.length; i++)
        {
            nej_hid_ent[i]=Math.random()*0.3+0.1;
        }

    }
    
    public void CoutOuter()
    {
        for(int i=0; i<hidden.length; i++)
        {
            hidden[i]=0;
            for (int j=0; j<enters.length;j++)
            {
                hidden[i]+=enters[j]*nej_ent_hid[j][i];
            }
            if(hidden[i]>0.5)hidden[i]=1;else hidden[i]=0;      
        }
        outer=0;
        for(int i =0; i<hidden.length;i++)
        {
            outer+=hidden[i]*nej_hid_ent[i];
        }
        if(outer>0.5)outer=1;else outer =0;
        
    }
    
    public void Study()
    {
        double[] err = new double[hidden.length];
        double GError =0;
        do{
            GError=0;
            for(int p =0; p<patterns.length; p++)
            {
                for(int i =0; i<enters.length;i++)
                {
                    enters[i] = patterns[p][i];
                }
                CoutOuter();
                double lError = answer[p]- outer;
                GError+=Math.abs(lError);
                for(int i =0;i<hidden.length;i++)
                {
                    err[i] = lError*nej_hid_ent[i];
                }
                
                for(int i =0; i<enters.length;i++)
                {
                    for(int j=0; j<hidden.length;j++)
                    {
                        nej_ent_hid[i][j]+=0.1*err[j]*enters[i];
                        
                    }
                }
                for(int i=0; i<hidden.length;i++)
                {
                    nej_hid_ent[i]+=0.1*lError*hidden[i];
                }
            }
        }while(GError!=0);
    }

 
    public static void main(String[] args) 
    {
       Xor xor =  new Xor ();
    }
    
}
