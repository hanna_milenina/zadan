//#include "stdafx.h"

#include <iostream>

using namespace std;

//#include <iostream.h>
#include <math.h>
#include <stdio.h>
//#include <iostream.h>
#include <stdlib.h>
#include <time.h>


const int CMaxInp=20;         //Максимальное число входов
const int CMaxOut=10;         //Максимальное число выходов
const int CMaxImages=200;     //Максимальное число образов
const double CEta=0.75;       //Темп обучения
const double CError=5.0e-4;   //Граница требуемой ошибки
const int CCounter=1000;      //Максимальное число итераций
const double CInitWeight=5.0; //Максимальное начальное значение случайных синаптических весов
const double CBiasNeuron=1.0; //Активность нейрона-порога


typedef double  TMatrix[CMaxInp][CMaxOut+1];
//Нулевой столбец содержит значения порогов
typedef double  TInpVector[CMaxInp];
typedef double  TOutVector[CMaxOut];


//Структура сети
struct  TPerceptron
		{
        int NInp;        //Число входов
        int NOut;        //Число выходов
        TInpVector Inp;  //Текущий вектор входов
        TOutVector Out;  //Текущий вектор выходов
        TMatrix W;       //Матрица связей
      };


//Запись в базе данных - обучающей выборке
struct  TBaseRecord
      {
        TInpVector X;
        TOutVector Y;
      } ;


//Структура базы данных
struct TBase
		{
        int NImages;  //Число обучающих образов
		  TBaseRecord Images[CMaxImages];
      };

TPerceptron VNet;
TBase VBase;
bool VOK;
double VError, VTemp, VDelta;
int VCounter, Vi, Vj, Vk;
FILE  *VFile;




double Sigmoid(double Z)
{
   return (1.0/(1.0+exp(-Z)));
}

 // Инициализация нейронной сети с 10 входами и одним выходом,
 // задание начальных случайных значений матрицы связей
void InitAll()

{
srand( (unsigned)time( NULL ) );

   int Li, Lj, Lk ;
        VNet.NInp=10;
        VNet.NOut=1;
        for(Li=0;Li<=VNet.NInp;Li++)
          {
           for(Lj=1;Lj<=VNet.NOut;Lj++)
             { 
              VNet.W[Li][Lj]=CInitWeight*((double)rand()/RAND_MAX-0.5);
              }
           }
			
    VOK=true;
}

void GetDataBase()
// Генерация обучающей выборки из 200 случайных образов.
 //  При определении правильного числа единиц используется прямой
//   подстчет *)
{
srand( (unsigned)time( NULL ) );
int Li, Lj, Lk;

    VOK=true;
    VBase.NImages=200;
        for(Li=1;Li<=VBase.NImages;Li++)
          { Lk=0;			
            for(Lj=1;Lj<=VNet.NInp;Lj++)
              {
               // Случайно 0 или 1 *)
                VBase.Images[Li].X[Lj]=rand()>RAND_MAX/2?1:0;
                //cout<<" "<<VBase.Images[Li].X[Lj];
                // Подсчет единиц *)
                if(VBase.Images[Li].X[Lj]>0)Lk=Lk+1;
               }
            // Выход равен единице, если в данном входном векторе
             //  число единиц больше числа нулей *)
            if(Lk>(VNet.NInp-Lk)) VBase.Images[Li].Y[1]=1;
                else VBase.Images[Li].Y[1]=0;
             //cout<<"     "<<VBase.Images[Li].Y[1];
          }
}

void main()
{  double VCorrect;
 cout<<"-----------------------------------------";
    VOK = true;

    // Инициализация с контролем ошибки
    srand( (unsigned)time( NULL ) );

    InitAll();

    if (!VOK)
         { cout<<"\n Ошибка инициализации"; }

     
    // Генерация базы данных
    VOK = true;
    GetDataBase();

    if (!VOK)
        {cout<<"\n Ошибка при генерации базы данных"; }

    // Цикл обучения
    VOK=true;
    VCounter=0;

          do
           {
              VError=0.0;
              //Цикл по обучающей выборке
              for (int Vi=1;Vi<=VBase.NImages;Vi++)
                {
                  //Подача очередного образа на входы сети
                  for (int Vj=1;Vj<=VNet.NInp;Vj++)
                    {VNet.Inp[Vj]=VBase.Images[Vi].X[Vj];}

                  //Цикл по нейронам. При аппаратной реализации
                  // будет выполняться параллельно !!!

                  for(int Vk=1;Vk<=VNet.NOut;Vk++)
                     { //Состояние очередного нейрона
                        VTemp=CBiasNeuron*VNet.W[0][Vk];
                        for (int Vj=1;Vj<=VNet.NInp;Vj++)
                           {VTemp=VTemp+VNet.Inp[Vj]*VNet.W[Vj][Vk];}

                        VNet.Out[Vk]=Sigmoid( VTemp );

                        //Накопление ошибки
                          VDelta= VBase.Images[Vi].Y[Vk]-VNet.Out[Vk];
                          VError= VError + 0.5*VDelta*VDelta;

                        //Обучение по дельта-правилу Розенблатта
                           VNet.W[0][Vk]= VNet.W[0][Vk]+CEta*CBiasNeuron*VDelta;

                          for ( Vj=1;Vj<=VNet.NInp;Vj++)
                            {VNet.W[Vj][Vk]=VNet.W[Vj][Vk] + CEta*VNet.Inp[Vj]*VDelta;}

                      }
                 }

               VCounter = VCounter + 1;
           }

        while ( (VCounter < CCounter) && (VError > CError));
    // }

  // Цикл завершен при достижении максимального числа
  // итераций или минимально достаточной ошибки

    printf("\nIter =%d  ", VCounter);
    printf("\nError=%lf\n ", VError) ;

        printf("P E R C E P T R O N>> (Test)\n");
        printf("-----------------------------------------------\n");
        printf(" Question               Answer   Correct answer  \n");
        printf("-----------------------------------------------\n");
        for (Vi=1;Vi<=CMaxImages/5;Vi++)
          {
            // Подача на вход случайного образа /
            Vk=0;
            for (Vj=1;Vj<=VNet.NInp;Vj++)
              {
                // Случайно 0 или 1
                VNet.Inp[Vj]=rand()>RAND_MAX/2?1:0;
                // Подсчет единиц /
                if (VNet.Inp[Vj]>0)
                Vk=Vk+1;
              }
            // Правильный ответ известен !
            if (Vk>(VNet.NInp-Vk))
              VCorrect=1.0;
            else VCorrect=0.0;
            // Ответ выдает нейросеть
            for (Vk=1;Vk<=VNet.NOut;Vk++)
            {
                VTemp=CBiasNeuron*VNet.W[0][Vk];
                for (Vj=1;Vj<=VNet.NInp;Vj++)
                {
                    VTemp=VTemp+VNet.Inp[Vj]*VNet.W[Vj][Vk];
                }
                VNet.Out[Vk]=Sigmoid(VTemp);
            }
            // Выдача результатов
            for (Vj=1;Vj<=VNet.NInp;Vj++)
                printf(" %d",(int)VNet.Inp[Vj]);
            printf("%6.2lf   %d\n",VNet.Out[1],(int)VCorrect);
        }


    cout<<"-------------------------------------------------";
	system("PAUSE");
}